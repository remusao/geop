

all:
	@make -C ./src

clean:
	@make -C ./src clean

distclean: clean
	@rm -frv *.pdf
